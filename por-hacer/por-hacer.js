const fs = require('fs');


let listadoPorHacer = [];

const saveDB = () => {
    let data = JSON.stringify(listadoPorHacer, null, 2);

    fs.writeFile('db/data.json', data, (err) => {
        if (err) throw new Err('No se pudo grabar', err);
    });
};


const loadDB = () => {
    try {
        listadoPorHacer = require('../db/data.json');
    } catch (error) {
        listadoPorHacer = [];
    }

};


const crear = (descripcion) => {

    loadDB();

    let porHacer = {
        descripcion,
        completado: false
    };

    listadoPorHacer.push(porHacer);
    saveDB();
    return porHacer;

};

const getListado = () => {
    loadDB();
    return listadoPorHacer;
};


const update = (descripcion, completado) => {
    loadDB();
    let index = listadoPorHacer.findIndex(tarea => tarea.descripcion === descripcion);

    if (index >= 0) {
        listadoPorHacer[index].completado = completado;
        saveDB();
        return true;
    } else {
        return false;
    }
};


const borrar = (descripcion) => {
    loadDB();
    let nuevoListado = listadoPorHacer.filter(tarea => {
        return tarea.descripcion !== descripcion;
    });

    if (listadoPorHacer.length === nuevoListado.length) {
        return false;
    } else {
        listadoPorHacer = nuevoListado;
        saveDB();
        return true;
    }
};

module.exports = {
    crear,
    getListado,
    update,
    borrar
};