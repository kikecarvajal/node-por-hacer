const color = require('colors');
const porHacer = require('./por-hacer/por-hacer');
const argumentos = require('./config/help').argumentos;
const fs = require('fs');


//console.log(argumentos);

let comando = argumentos._[0];

let dir = "./db";

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

switch (comando) {
    case 'create':
        let tarea = porHacer.crear(argumentos.descripcion);
        console.log(tarea);
        break;

    case 'list':
        let listado = porHacer.getListado();
        for (let tarea of listado) {
            console.log('======= Por hacer ========='.green);
            console.log(tarea.descripcion);
            console.log('Estado:', tarea.completado);
        }
        break;

    case 'update':
        let update = porHacer.update(argumentos.descripcion, argumentos.completado);
        console.log(update);
        break;

    case 'delete':
        let borrar = porHacer.borrar(argumentos.descripcion);
        console.log(borrar);
        break;

    default:
        console.log(`El Comando "${comando} no es valida"`.red);
}