const descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripcion de la tarea por hacer'
};

const completado = {
    default: true,
    alias: 'c',
    desc: 'Marca como completado o pendiente la tarea'
};


const argumentos = require('yargs')
    .command('create', 'Crear un elemento por hacer', {
        descripcion
    })
    .command('update', 'Actualizar el estado completado de una tarea', {
        descripcion,
        completado
    })
    .command('delete', 'borrar una tarea', {
        descripcion
    })
    .example('node app create -d "text"'.green, 'Crea una tarea por hacer y poner su descripcion')
    .example('node app update -d "text" -c '.yellow, 'Actualiza un tarea por hacer')
    .example('node app delete -d "text"'.red, 'Borrado de tarea')
    .help()
    .epilog('copyright 2020'.red)
    .argv;


module.exports = {
    argumentos
};